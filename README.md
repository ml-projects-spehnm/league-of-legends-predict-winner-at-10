# League of Legends - Predict winner at 10 minutes ingame

## Description

This project uses a neural network to predict the winner of a League of Legends game after 10 minutes ingame given the current data. It uses the "League of Legends Diamond Ranked Games (10 min)"-Dataset by Kaggle User Yi Lan Ma, avaible on https://www.kaggle.com/datasets/bobbyscience/league-of-legends-diamond-ranked-games-10-min. 

## Disclaimer
The current version ist just a framework with no claim to be usable right now. I use it to learn and train ML. Future versions will (hopefully) show improvement. 

## Context, content and glossary
You can find the data's context, content and glossary under: https://www.kaggle.com/datasets/bobbyscience/league-of-legends-diamond-ranked-games-10-min. I can only recommend the data analysis by Kaggle user Dr.Penguin: https://www.kaggle.com/code/xiyuewang/lol-how-to-win.

## License & Legal
The dataset used is under CC0: Public Domain.  Therefore this uses the same license.

THE PROJECT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.